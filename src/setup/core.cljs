(ns setup.core
  (:require [cljs.nodejs :as nodejs]))

(def ^:private path (nodejs/require "path"))

(def ^:private electron (nodejs/require "electron"))

(def ^:private browser-window (.-BrowserWindow electron))
(def ^:private menu (.-Menu electron))

(def ^:private crash-reporter (.-crashReporter electron))

(def ^:private os (nodejs/require "os"))

(def ^:private darwin? (= "darwin" js/process.platform))

(def ^:private is-development?
  (boolean (or (.-defaultApp js/process)
               (re-matches #"[\\/]electron-prebuilt[\\/]"
                           (.-execPath js/process))
               (re-matches #"[\\/]electron[\\/]" (.-execPath js/process)))))

(def ^:private *win* (atom nil))

(def ^:private app (.-app electron))

(defn- init-menu []
  (let [name (.getName app)
        template
        (cond-> []
          (not darwin?)
          (conj {:label   "&File"
                 :submenu [{:label       "Preferences"
                            :accelerator "Ctrl+,"}
                           {:type "separator"}
                           {:label       "Quit"
                            :accelerator "Ctrl+Q"
                            :click       (fn [] (.quit app))}]})
          darwin?
          (conj {:label   name
                 :submenu [{:label (str "About " name)
                            :role  "about"}
                           {:type "separator"}
                           {:label   "Services"
                            :role    "services"
                            :submenu []}
                           {:type "separator"}
                           {:label       (str "Hide " name)
                            :accelerator "Command+H"
                            :role        "hide"}
                           {:label       "Hide Others"
                            :accelerator "Command+Shift+H"
                            :role        "hideothers"}
                           {:label "Show All"
                            :role  "unhide"}
                           {:type "separator"}
                           {:label       "Quit"
                            :accelerator "Command+Q"
                            :click       (fn [] (.quit app))}]})
          :always
          (-> (concat [{:label   "&Edit"
                        :submenu [{:label       "Undo"
                                   :accelerator "CmdOrCtrl+Z"
                                   :role        "undo"}
                                  {:label       "Redo"
                                   :accelerator "Shift+CmdOrCtrl+Z"
                                   :role        "redo"}
                                  {:type "separator"}
                                  {:label       "Cut"
                                   :accelerator "CmdOrCtrl+X"
                                   :role        "cut"}
                                  {:label       "Copy"
                                   :accelerator "CmdOrCtrl+C"
                                   :role        "copy"}
                                  {:label       "Paste"
                                   :accelerator "CmdOrCtrl+V"
                                   :role        "paste"}
                                  {:label       "Select All"
                                   :accelerator "CmdOrCtrl+A"
                                   :role        "selectall"}]}]
                      (when is-development?
                        [{:label   "&View"
                          :submenu [{:label       "Reload"
                                     :accelerator "CmdOrCtrl+R"
                                     :click       (fn [_ focusedWindow]
                                                    (when focusedWindow
                                                      (.reload focusedWindow)))}
                                    {:label       "Toggle Full Screen"
                                     :accelerator (if darwin?
                                                    "Ctrl+Command+F"
                                                    "F11")
                                     :click
                                     (fn [_ focusedWindow]
                                       (when focusedWindow
                                         (let [full?
                                               (.isFullScreen focusedWindow)]
                                           (.setFullScreen focusedWindow
                                                           (not full?)))))}
                                    {:label       "Toggle Developer Tools"
                                     :accelerator (if darwin?
                                                    "Alt+Command+I"
                                                    "Ctrl+Shift+I")
                                     :click       (fn [_ focusedWindow]
                                                    (when focusedWindow
                                                      (.toggleDevTools
                                                       focusedWindow)))}]}])
                      [{:label   "&Window"
                        :role    "window"
                        :submenu [{:label       "Minimize"
                                   :accelerator "CmdOrCtrl+M"
                                   :role        "minimize"}
                                  {:label       "Close"
                                   :accelerator "CmdOrCtrl+W"
                                   :role        "close"}]}])
              vec)

          darwin?
          (update-in [3 :submenu] conj
                     {:type "separator"}
                     {:label "Bring All to Front"
                      :role  "front"})

          :always
          clj->js)]
    (.setApplicationMenu menu (.buildFromTemplate menu template))))

(defn -main []
  (.start crash-reporter (clj->js {:companyName "Your Company Name"
                                   :submitURL   "http://example.com/"}))

  ;; error listener
  (.on nodejs/process "error"
       (fn [err] (.log js/console err)))

  ;; window all closed listener
  (.on app "window-all-closed"
       (fn [] (if (not= (.-platform nodejs/process) "darwin")
                (.quit app))))

  ;; ready listener
  (.on app "ready"
       (fn []
         (init-menu)

         (reset! *win* (browser-window. (clj->js {:width 800 :height 600})))

         ;; when no optimize comment out
         (.loadURL @*win* (str "file://" (.resolve path (js* "__dirname") "../index.html")))
         ;; when no optimize uncomment
         ;; (.loadURL @*win* (str "file://" (.resolve path (js* "__dirname") "../../../index.html")))

         (.on @*win* "closed" (fn [] (reset! *win* nil))))))

(nodejs/enable-util-print!)

;;; "Linux" or "Darwin" or "Windows_NT"
(.log js/console (str "Starting application (OS: " (.type os) ")"))

(set! *main-cli-fn* -main)
