(ns setup-front.views
  (:require [re-com.core :as re-com]
            [re-frame.core :as rf]
            [setup-front.events :as events]
            [setup-front.subs :as subs]))

(defn- selected-item []
  (let [selected-item (rf/subscribe [::subs/selected-item])]
    [re-com/label :label (str "Currently selected item: "
                              (with-out-str (print @selected-item)))]))

(defn- dropdown []
  (let [selected-item (rf/subscribe [::subs/selected-item])]
    [re-com/h-box
     :gap "10px"
     :align :center
     :children
     [[re-com/label :label "Example dropdown"]
      [re-com/single-dropdown
       :placeholder "Select an item"
       :choices [{:id 1 :label "Item 1"}
                 {:id 2 :label "Item 2"}
                 {:id 3 :label "Item 3"}]
       :model selected-item
       :on-change #(rf/dispatch [::events/set-selected-item %])
       :width "200px"]]]))

(defn content []
  [re-com/v-box
   :gap "10px"
   :children
   [[:h1.animated.fadeInLeft (str "Hello, " @(rf/subscribe [::subs/name]))]
    [re-com/button
     :class "btn-info"
     :label "What's my name?"
     :on-click #(rf/dispatch [::events/toggle-name])]
    [selected-item]
    [dropdown]]])

(defn main-panel []
  [:div.container
   [content]])
