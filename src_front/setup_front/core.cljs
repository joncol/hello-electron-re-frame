(ns setup-front.core
  (:require [reagent.core :as reagent :refer [atom]]
            [re-frame.core :as rf]
            [setup-front.events :as events]
            [setup-front.views :as views]))

(defn mount-root [setting]
  (reagent/render [views/main-panel]
                  (.getElementById js/document "app"))
  )

(defn init! [setting]
  (rf/dispatch-sync [::events/initialize-db])
  (mount-root setting))
