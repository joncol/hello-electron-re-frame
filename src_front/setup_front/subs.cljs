(ns setup-front.subs
  (:require [re-frame.core :as rf]))

(rf/reg-sub
  ::name
  (fn [db _]
    (:name db)))

(rf/reg-sub
  ::selected-item
  (fn [db _]
    (:selected-item db)))
