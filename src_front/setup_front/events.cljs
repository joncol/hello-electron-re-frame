(ns setup-front.events
  (:require [cljs.nodejs :as nodejs]
            [re-frame.core :as rf]
            [setup-front.db :as db]))

(rf/reg-event-db
  ::initialize-db
  (fn [_ _]
    db/default-db))

(def ^:private os (nodejs/require "os"))

(defn- whoami []
  (.-username (.userInfo os)))

(rf/reg-event-db
  ::toggle-name
  (rf/path [:name])
  (fn [name _]
    (if (= (whoami) name) "world" (whoami))))

(rf/reg-event-db
  ::set-selected-item
  (rf/path [:selected-item])
  (fn [old-value [_ new-value]]
    new-value))
