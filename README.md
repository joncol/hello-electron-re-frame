# Electron + re-frame Sample Project

This is a basic `ClojureScript` hello-world project that uses `Electron` and
`re-frame`. It also has `re-frame-10x` and `Cider` integration.

Most of this was taken from
https://github.com/soulflyer/electron-re-frame-example. Some updates were
applied to use the latest version of Electron and other libraries.

Also scss processing was added.

## Prerequisites

Install the Grunt command line tools:

```bash
npm install -g grunt-cli
```

## Building

To build a development version of the application:

```bash
lein do descjop-init, descjop-externs-dev, descjop-once-dev
lein descjop-figwheel # Or `cider-jack-in-clojurescript` from Emacs
```

On Windows, replace `descjop-init` with `descjop-init-win`, in the above.

### Style Compilation

To compile [sass](https://github.com/tuhlmann/lein-sass) sources and then watch
for changes and recompile until interrupted, run:
```
lein sass auto
```

## Running

To start the development version of the application:

```bash
electron/electron app/dev
```

## Building for Release

The command to build a production version of the application depends on your
target platform.

```bash
lein descjop-uberapp-linux
lein descjop-uberapp-osx
lein descjop-uberapp-app-store
lein descjop-uberapp-win64
lein descjop-uberapp-win32
```
